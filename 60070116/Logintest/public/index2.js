const auth = firebase.auth();
const idList = document.querySelector('#id-list')
// call firestore
const firestore = firebase.firestore()

const settings = {
  /* your settings... */
  timestampsInSnapshots: true
};
firestore.settings(settings);

auth.onAuthStateChanged(function (user) {

  if (user) {
    // User is signed in.

    document.getElementById("user_div").style.display = "block";
    document.getElementById("login_div").style.display = "none";

    // get current user data
    firestore
      .collection('users') // from collection users
      .doc(user.uid) // doc of current user by get user ID (uid)
      .get()
      .then(function (docs) {
        // set user as data that collect from firebase
        var user = docs.data();

        var firstname_id = user.firstname;
        var lastname_id = user.lastname;
        var email_id = user.email;
        document.getElementById("user_para").innerHTML = "Welcome User : " + email_id + " " + firstname_id + " " + lastname_id;
      }).catch(() => { })

      // function renderID(doc){
      //   let li = document.createElement('li');
      //   let stuid = document.createElement('span');
      //   li.setAttribute('data-id', doc.id);
      //   stuid.textContent = doc.id;

      //   li.appendChild(stuid);
        
      //   idList.appendChild(li);
      // }
        

      //         firestore.collection("users").doc(user.uid).collection('classes').doc('cid').collection('students').get().then((snapshot) => {
      //   snapshot.docs.forEach(doc => {
      //     renderID(doc);
      //   })
      // })  ลองดึงไอดีอันเก่า
      

  } else {
    // No user is signed in.

    document.getElementById("user_div").style.display = "none";
    document.getElementById("login_div").style.display = "block";

  }
});

function signUp() {
  var titlename = document.getElementById("titlename").value;
  var firstname = document.getElementById("firstname").value;
  var lastname = document.getElementById("lastname").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;

  auth.createUserWithEmailAndPassword(email, password).then((authUser) => {
    let newUser = authUser.user


    // set new user data
    firestore
      .collection('users') // to collection users
      .doc(newUser.uid) // in doc user ID (uid)
      .set({
        email: email,
        titlename: titlename,
        firstname: firstname,
        lastname: lastname,
      })
    // firestore
    //   .collection('classes')
    //   .add({
    //     classID: 0, ,pa
    //     classNameEN: "",
    //     classNameTH: "",
    //     year: 0,
    //     semester: 0,
    //     studyDay: firebase.firestore.Timestamp.fromDate(new Date("December 10, 1815")), ตัวอย่างวัน
    //     startTime: "",
    //     endTime: "",
    //   }) สร้างตอนเพิ่ม class
    // firestore
    //   .collection('students')
    //   .add({
    //     firstName: "",
    //     lastName: ""
    //   }) สร้่างตอนเพิ่มนศ.
    // firestore
    //   .collection('attendance')
    //   .add({
    //     uid : newUser.uid,
    //     date: "",
    //     startTime: "",
    //     endTime: ""
    //   })  สร้างตอนกดเช็คชื่อ
    // firestore
    //   .collection('attendance')
    //   .doc(aid) สร้างเป็นตัวแปรเก็บไว้แล้วค่อยเอามาใส่
    //   .collection('students')
    //   .set({
    //     timeStamp: "",
    //     status: ""
    //   }) สร้างตอนแสกนเชคชื่อ
      .then(() => {
        alert('created!')
      }).catch((e) => {
        console.log(e)
      })

  }).catch(function (error) {
    // Handle Errors here.
    console.log(error.code, error.message)
    // ...
  });
}

function login() {

  var userEmail = document.getElementById("email_field").value;
  var userPass = document.getElementById("password_field").value;

  firebase.auth().signInWithEmailAndPassword(userEmail, userPass).catch(function (error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;

    window.alert("Error : " + errorMessage);

    // ...
  });

}

function logout() {
  firebase.auth().signOut();
}

function addStudent() {
      firestore
      .collection('students')
      .add({
        firstName: "",
        lastName: ""
      })
}

function addSubject() {
  var nowUid = auth.currentUser.uid;
  firestore
      .collection('classes')
      .add({
        uid: nowUid,
        classID: 0, 
        classNameEN: "",
        classNameTH: "",
        year: 0,
        semester: 0,
        studyDay: firebase.firestore.Timestamp.fromDate(new Date("December 10, 1815")), //ตัวอย่างวัน
        startTime: "",
        endTime: "",
        students: []
      })
}


// function createClass(){
//   var code = "6070116";
//  firebase.auth().onAuthStateChanged(function (user) {
//   if (user) {
//     this.user = user.uid;
//     firestore.collection("users").doc([user.uid]).collection('classes').doc('students').update({
//       [code]: {
//       firstName: "",
//       lastName: ""
//       }
      
//     });
//     alert("check "+code)
//   } else {
//     console.log("no user")
//   }
// }) 
// }


